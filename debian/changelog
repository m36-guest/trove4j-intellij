libtrove-intellij-java (1.0.20181211-1) unstable; urgency=medium

  * Package the Jetbrains upstream source.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 03 Feb 2019 15:19:22 +0100

trove3 (3.0.3-5) unstable; urgency=medium

  [ Steffen Möller ]
  * Team upload.
  * Update watch - project moved to bitbucket
  * Using Files-Excluded in d/copyright, formatting.

  [ Markus Koschany ]
  * Declare compliance with Debian Policy 4.2.1.
  * Add no-CORBA-JDK11.patch. Remove the test failing with OpenJDK 11.
    (Closes: #912265)

 -- Markus Koschany <apo@debian.org>  Mon, 29 Oct 2018 20:02:06 +0100

trove3 (3.0.3-4) unstable; urgency=medium

  * Team upload.
  * Moved the javadoc to /usr/share/doc/libtrove3-java/api/
  * Moved the package to Git
  * Standards-Version updated to 4.1.4
  * Switch to debhelper level 11

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 18 Apr 2018 18:16:41 +0200

trove3 (3.0.3-3) unstable; urgency=medium

  * Team upload.
  * Build target release and ship all generated classes like
    TIntObjectHashMap.class.
  * Enable the test suite. Add ant-optional and junit4 to Build-Depends.
  * Remove Michael Koch from Uploaders. He is not active anymore.
  * Use canonical Vcs-URI. Use https.
  * Rename README.Debian-source to README.source.

 -- Markus Koschany <apo@debian.org>  Sat, 30 Jan 2016 15:54:00 +0100

trove3 (3.0.3-2) unstable; urgency=low

  * Bump to policy version 3.9.6 (no changes)
  * Do not include user name in Manifest, to make build easier to
    reproduce (via reproducible.debian.net)
  * Switch build system to DH7 + javahelper instead of CDBS
  * Install a pom file into /usr/share/maven-repo to allow secure builds
    using maven in offline mode.
  * Jars were renamed to trove4j due to compatibility with maven,
    install legacy symlinks named trove-3.jar and trove-3.0.3.jar
    (Used inconsistenly by upstream, too.)

 -- Erich Schubert <erich@debian.org>  Fri, 01 May 2015 14:19:01 +0200

trove3 (3.0.3-1) unstable; urgency=low

  * New upstream bug fix release.
  * Fixes: ArrayIndexOutOfBoundsException in TIntHashSet.retainAll
  * Fixes: no_entry_value initialized incorrectly

 -- Erich Schubert <erich@debian.org>  Sun, 15 Jul 2012 12:27:10 +0200

trove3 (3.0.2-1) unstable; urgency=low

  * New upstream major version. (Closes: #671295)
  * Rename package to trove3, to allow parallel installation
    of API versions 2 and 3 (which is intended by upstream).
  * License is LGPL 2.1 now, refresh copyright statements
  * Slightly updated orig-tar.sh file for version 3.
  * Set -Dversion.number during build.
  * Watch only for trove 3 tarballs.
  * Make lintian happier: do not depend on a JDK
  * Update policy version, no changes.

 -- Erich Schubert <erich@debian.org>  Thu, 03 May 2012 07:22:41 +0200

trove (2.1.0-2) unstable; urgency=low

  [ Thierry Carrez ]
  * Set minimal depend to default-jre-headless | java5-runtime-headless
    since we build Java5 code.

  [ Torsten Werner ]
  * Remove Arnaud from Uploaders list.
  * Switch to source format 3.0.
  * Update Standards-Version: 3.8.4.

 -- Torsten Werner <twerner@debian.org>  Wed, 05 May 2010 07:40:42 +0200

trove (2.1.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Koch <konqueror@gmx.de>  Fri, 02 Oct 2009 22:09:29 +0200

trove (2.0.4-2) unstable; urgency=low

  * (Build-)Depends on default-jdk (Closes: #526301)
  * Build-Depends on debhelper >= 7.
  * Let all packages depend on ${misc:Depends}.
  * Moved package to section 'java'.
  * Added myself to Uploaders.
  * Updated Standards-Version to 3.8.3.

 -- Michael Koch <konqueror@gmx.de>  Wed, 16 Sep 2009 22:54:50 +0200

trove (2.0.4-1) unstable; urgency=low

  * new upstream release
  * Bump up Standards-Version: 3.8.0 (no changes needed).

 -- Torsten Werner <twerner@debian.org>  Sun, 24 Aug 2008 23:30:31 +0200

trove (2.0.3-1) unstable; urgency=low

  * new upstream release
  * Fix Author field in doc-base file.

 -- Torsten Werner <twerner@debian.org>  Mon, 07 Apr 2008 23:32:29 +0200

trove (2.0.2-1) unstable; urgency=low

  * new upstream release
  * Remove reference to substance in README.Debian-source.
  * Clean up the debian directory.
  * Change Standards-Version: 3.7.3.
  * Add Homepage and Vcs headers to debian/control.

 -- Torsten Werner <twerner@debian.org>  Sun, 16 Dec 2007 18:44:02 +0100

trove (2.0.1.dfsg.1-1) unstable; urgency=low

  * Initial Release (closes: #418678).

 -- Arnaud Vandyck <avdyk@debian.org>  Mon, 16 Jul 2007 10:43:45 +0200
